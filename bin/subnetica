#!/usr/bin/env ruby
# frozen_string_literal: true
# sharable_constant_value: literal

require 'ipaddr'
require 'subnetica'

if ARGV.length != 1
  puts 'Usage: subnetica [ip address]'
  exit!
end

ipaddr_str = ARGV[0].dup
ipaddr_str.strip!

begin
  ipaddr = IPAddr.new ipaddr_str
rescue IPAddr::InvalidAddressError
  puts 'Invalid address'
  exit!
end

attributes, flags = Subnetica.getinfo ipaddr

max_len = 0
attributes.each_key do |key|
  max_len = key.length if key.length > max_len
end

length = max_len + 1
attributes.each_pair do |key, value|
  puts "#{key}#{' ' * (length - key.length)}= #{value}"
end

puts "Properties#{' ' * (length - 10)}="
flags.each do |flag|
  if flag.is_a? Array
    flag.each do |sub_flag|
      puts "\t\t+ #{sub_flag}"
    end
  else
    puts "\t- #{flag}"
  end
end
