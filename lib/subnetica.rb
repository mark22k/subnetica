# frozen_string_literal: true
# sharable_constant_value: literal

module Subnetica

  require 'ipaddr'
  require 'meshname'

  ADDRESSES_4 = [
    [IPAddr.new('0.0.0.0/8'), 'Current network'],
    [IPAddr.new('10.0.0.0/8'), 'Private address'],
    [IPAddr.new('100.64.0.0/10'), 'CG-NAT'],
    [IPAddr.new('127.0.0.0/8'), 'Loopback address'],
    [IPAddr.new('169.254.0.0/16'), 'Link-local address'],
    [IPAddr.new('172.16.0.0/12'), 'Private address'],
    [IPAddr.new('192.0.0.0/24'), 'IETF Protocol Assignments'],
    [IPAddr.new('192.0.2.0/24'), 'TEST-NET-1 (Documentation prefix)'],
    [IPAddr.new('192.88.99.0/24'), '6to4 Relay Anycast'],
    [IPAddr.new('192.88.99.2/32'), '6a44 Relay Anycast'],
    [IPAddr.new('192.168.0.0/16'), 'Private address'],
    [IPAddr.new('198.18.0.0/15'), 'benchmark testing of inter-network communication'],
    [IPAddr.new('198.51.100.0/24'), 'TEST-NET-2 (Documentation prefix)'],
    [IPAddr.new('203.0.113.0/24'), 'TEST-NET-3 (Documentation prefix)'],
    [IPAddr.new('224.0.0.0/4'), 'Multicast address'],
    [IPAddr.new('233.252.0.0/24'), 'MCAST-TEST-NET (Documentation prefix)'],
    [IPAddr.new('240.0.0.0/4'), 'Reserved for future use'],
    [IPAddr.new('239.255.255.250/32'), 'SSDP/UPnP'],
    [IPAddr.new('255.255.255.255/32'), 'Limited broadcast'],

    # Allocations
    [IPAddr.new('1.0.0.0/8'), 'APNIC'],
    [IPAddr.new('2.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('3.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('4.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('5.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('6.0.0.0/8'), 'Army Information Systems Center'],
    [IPAddr.new('7.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('8.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('9.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('11.0.0.0/8'), 'DoD Intel Information Systems'],
    [IPAddr.new('12.0.0.0/8'), 'AT&T Bell Laboratories'],
    [IPAddr.new('13.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('14.0.0.0/8'), 'APNIC'],
    [IPAddr.new('15.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('16.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('17.0.0.0/8'), 'Apple Computer Inc.'],
    [IPAddr.new('18.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('19.0.0.0/8'), 'Ford Motor Company'],
    [IPAddr.new('20.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('21.0.0.0/8'), 'DDN-RVN'],
    [IPAddr.new('22.0.0.0/8'), 'Defense Information Systems Agency'],
    [IPAddr.new('23.0.0.0/8'), 'ARIN'],
    [IPAddr.new('24.0.0.0/8'), 'ARIN'],
    [IPAddr.new('25.0.0.0/8'), 'Administered by RIPE NCC'],
    [IPAddr.new('26.0.0.0/8'), 'Defense Information Systems Agency'],
    [IPAddr.new('27.0.0.0/8'), 'APNIC'],
    [IPAddr.new('28.0.0.0/8'), 'DSI-North'],
    [IPAddr.new('29.0.0.0/8'), 'Defense Information Systems Agency'],                                   
    [IPAddr.new('30.0.0.0/8'), 'Defense Information Systems Agency'],                                   
    [IPAddr.new('31.0.0.0/8'), 'RIPE NCC'],                                   
    [IPAddr.new('32.0.0.0/8'), 'Administered by ARIN'],                                   
    [IPAddr.new('33.0.0.0/8'), 'DLA Systems Automation Center'],                                   
    [IPAddr.new('34.0.0.0/8'), 'Administered by ARIN'],                                   
    [IPAddr.new('35.0.0.0/8'), 'Administered by ARIN'],                                   
    [IPAddr.new('36.0.0.0/8'), 'APNIC'],                                   
    [IPAddr.new('37.0.0.0/8'), 'RIPE NCC'],                                   
    [IPAddr.new('38.0.0.0/8'), 'PSINet, Inc.'],                                   
    [IPAddr.new('39.0.0.0/8'), 'APNIC'],                                   
    [IPAddr.new('40.0.0.0/8'), 'Administered by ARIN'],                                   
    [IPAddr.new('41.0.0.0/8'), 'AFRINIC'],                                   
    [IPAddr.new('42.0.0.0/8'), 'APNIC'],
    [IPAddr.new('43.0.0.0/8'), 'Administered by APNIC'],
    [IPAddr.new('44.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('45.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('46.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('47.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('48.0.0.0/8'), 'Prudential Securities Inc.'],
    [IPAddr.new('49.0.0.0/8'), 'APNIC'],
    [IPAddr.new('50.0.0.0/8'), 'ARIN'],
    [IPAddr.new('51.0.0.0/8'), 'Administered by RIPE NCC'],
    [IPAddr.new('52.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('53.0.0.0/8'), 'Daimler AG'],
    [IPAddr.new('54.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('55.0.0.0/8'), 'DoD Network Information Center'],
    [IPAddr.new('56.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('57.0.0.0/8'), 'Administered by RIPE NCC'],
    [IPAddr.new('58.0.0.0/8'), 'APNIC'],
    [IPAddr.new('59.0.0.0/8'), 'APNIC'],
    [IPAddr.new('60.0.0.0/8'), 'APNIC'],
    [IPAddr.new('61.0.0.0/8'), 'APNIC'],
    [IPAddr.new('62.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('63.0.0.0/8'), 'ARIN'],
    [IPAddr.new('64.0.0.0/8'), 'ARIN'],
    [IPAddr.new('65.0.0.0/8'), 'ARIN'],
    [IPAddr.new('66.0.0.0/8'), 'ARIN'],
    [IPAddr.new('67.0.0.0/8'), 'ARIN'],
    [IPAddr.new('68.0.0.0/8'), 'ARIN'],
    [IPAddr.new('69.0.0.0/8'), 'ARIN'],
    [IPAddr.new('70.0.0.0/8'), 'ARIN'],
    [IPAddr.new('71.0.0.0/8'), 'ARIN'],
    [IPAddr.new('72.0.0.0/8'), 'ARIN'],
    [IPAddr.new('73.0.0.0/8'), 'ARIN'],
    [IPAddr.new('74.0.0.0/8'), 'ARIN'],
    [IPAddr.new('75.0.0.0/8'), 'ARIN'],
    [IPAddr.new('76.0.0.0/8'), 'ARIN'],
    [IPAddr.new('77.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('78.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('79.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('80.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('81.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('82.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('83.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('84.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('85.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('86.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('87.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('88.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('89.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('90.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('91.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('92.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('93.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('94.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('95.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('96.0.0.0/8'), 'ARIN'],
    [IPAddr.new('97.0.0.0/8'), 'ARIN'],
    [IPAddr.new('98.0.0.0/8'), 'ARIN'],
    [IPAddr.new('99.0.0.0/8'), 'ARIN'],
    [IPAddr.new('100.0.0.0/8'), 'ARIN'],
    [IPAddr.new('101.0.0.0/8'), 'APNIC'],
    [IPAddr.new('102.0.0.0/8'), 'AFRINIC'],
    [IPAddr.new('103.0.0.0/8'), 'APNIC'],
    [IPAddr.new('104.0.0.0/8'), 'ARIN'],
    [IPAddr.new('105.0.0.0/8'), 'AFRINIC'],
    [IPAddr.new('106.0.0.0/8'), 'APNIC'],
    [IPAddr.new('107.0.0.0/8'), 'ARIN'],
    [IPAddr.new('108.0.0.0/8'), 'ARIN'],
    [IPAddr.new('109.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('110.0.0.0/8'), 'APNIC'],
    [IPAddr.new('111.0.0.0/8'), 'APNIC'],
    [IPAddr.new('112.0.0.0/8'), 'APNIC'],
    [IPAddr.new('113.0.0.0/8'), 'APNIC'],
    [IPAddr.new('114.0.0.0/8'), 'APNIC'],
    [IPAddr.new('115.0.0.0/8'), 'APNIC'],
    [IPAddr.new('116.0.0.0/8'), 'APNIC'],
    [IPAddr.new('117.0.0.0/8'), 'APNIC'],
    [IPAddr.new('118.0.0.0/8'), 'APNIC'],
    [IPAddr.new('119.0.0.0/8'), 'APNIC'],
    [IPAddr.new('120.0.0.0/8'), 'APNIC'],
    [IPAddr.new('121.0.0.0/8'), 'APNIC'],
    [IPAddr.new('122.0.0.0/8'), 'APNIC'],
    [IPAddr.new('123.0.0.0/8'), 'APNIC'],
    [IPAddr.new('124.0.0.0/8'), 'APNIC'],
    [IPAddr.new('125.0.0.0/8'), 'APNIC'],
    [IPAddr.new('126.0.0.0/8'), 'APNIC'],
    [IPAddr.new('128.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('129.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('130.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('131.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('132.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('133.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('134.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('135.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('136.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('137.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('138.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('139.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('140.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('141.0.0.0/8'), 'Administered by RIPE NCC'],
    [IPAddr.new('142.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('143.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('144.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('145.0.0.0/8'), 'Administered by RIPE NCC'],
    [IPAddr.new('146.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('147.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('148.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('149.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('150.0.0.0/8'), 'Administered by APNIC'],
    [IPAddr.new('151.0.0.0/8'), 'Administered by RIPE NCC'],
    [IPAddr.new('152.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('153.0.0.0/8'), 'Administered by APNIC'],
    [IPAddr.new('154.0.0.0/8'), 'Administered by AFRINIC'],
    [IPAddr.new('155.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('156.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('157.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('158.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('159.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('160.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('161.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('162.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('163.0.0.0/8'), 'Administered by APNIC'],
    [IPAddr.new('164.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('165.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('166.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('167.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('168.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('169.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('170.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('171.0.0.0/8'), 'Administered by APNIC'],

    # Exclude 172.16.0.0/12
    [IPAddr.new('172.0.0.0/12'), 'Administered by ARIN'],
    [IPAddr.new('172.32.0.0/11'), 'Administered by ARIN'],
    [IPAddr.new('172.64.0.0/10'), 'Administered by ARIN'],
    [IPAddr.new('172.128.0.0/9'), 'Administered by ARIN'],

    [IPAddr.new('173.0.0.0/8'), 'ARIN'],
    [IPAddr.new('174.0.0.0/8'), 'ARIN'],
    [IPAddr.new('175.0.0.0/8'), 'APNIC'],
    [IPAddr.new('176.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('177.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('178.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('179.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('180.0.0.0/8'), 'APNIC'],
    [IPAddr.new('181.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('182.0.0.0/8'), 'APNIC'],
    [IPAddr.new('183.0.0.0/8'), 'APNIC'],
    [IPAddr.new('184.0.0.0/8'), 'ARIN'],
    [IPAddr.new('185.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('186.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('187.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('188.0.0.0/8'), 'Administered by RIPE NCC'],
    [IPAddr.new('189.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('190.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('191.0.0.0/8'), 'Administered by LACNIC'],
    [IPAddr.new('192.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('193.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('194.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('195.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('196.0.0.0/8'), 'Administered by AFRINIC'],
    [IPAddr.new('197.0.0.0/8'), 'AFRINIC'],
    [IPAddr.new('198.0.0.0/8'), 'Administered by ARIN'],
    [IPAddr.new('199.0.0.0/8'), 'ARIN'],
    [IPAddr.new('200.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('201.0.0.0/8'), 'LACNIC'],
    [IPAddr.new('202.0.0.0/8'), 'APNIC'],
    [IPAddr.new('203.0.0.0/8'), 'APNIC'],
    [IPAddr.new('204.0.0.0/8'), 'ARIN'],
    [IPAddr.new('205.0.0.0/8'), 'ARIN'],
    [IPAddr.new('206.0.0.0/8'), 'ARIN'],
    [IPAddr.new('207.0.0.0/8'), 'ARIN'],
    [IPAddr.new('208.0.0.0/8'), 'ARIN'],
    [IPAddr.new('209.0.0.0/8'), 'ARIN'],
    [IPAddr.new('210.0.0.0/8'), 'APNIC'],
    [IPAddr.new('211.0.0.0/8'), 'APNIC'],
    [IPAddr.new('212.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('213.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('214.0.0.0/8'), 'US-DOD'],
    [IPAddr.new('215.0.0.0/8'), 'US-DOD'],
    [IPAddr.new('216.0.0.0/8'), 'ARIN'],
    [IPAddr.new('217.0.0.0/8'), 'RIPE NCC'],
    [IPAddr.new('218.0.0.0/8'), 'APNIC'],
    [IPAddr.new('219.0.0.0/8'), 'APNIC'],
    [IPAddr.new('220.0.0.0/8'), 'APNIC'],
    [IPAddr.new('221.0.0.0/8'), 'APNIC'],
    [IPAddr.new('222.0.0.0/8'), 'APNIC'],
    [IPAddr.new('223.0.0.0/8'), 'APNIC'],

    # Overlay networks
    [IPAddr.new('172.20.0.0/14'), 'dn42 network'],
    [IPAddr.new('172.20.0.0/24'), 'dn42 anycast'],
    [IPAddr.new('172.21.0.0/24'), 'dn42 anycast'],
    [IPAddr.new('172.22.0.0/24'), 'dn42 anycast'],
    [IPAddr.new('172.23.0.0/24'), 'dn42 anycast'],
    [IPAddr.new('172.20.240.0/20'), 'dn42 network transfer networks'],
    [IPAddr.new('172.22.240.0/20'), 'dn42 network transfer networks'],
    [IPAddr.new('172.31.0.0/16'), 'ChaosVPN'],
    [IPAddr.new('10.100.0.0/14'), 'ChaosVPN'],
    [IPAddr.new('10.127.0.0/16'), 'NeoNetwork'],
    [IPAddr.new('172.24.0.0/16'), 'router.city'],

    # Connectivity services with free client
    [IPAddr.new('100.64.0.0/10'), 'Default Tailscale subnet'],
    [IPAddr.new('100.80.0.0/12'), 'Default CloudConnexa WPC Domain Routing subnet'],
    [IPAddr.new('100.96.0.0/11'), 'Default CloudConnexa WPC subnet']
  ].freeze

  ADDRESSES_6 = [
    [IPAddr.new('::/96'), 'IPv4-mapped'],
    [IPAddr.new('::/128'), 'Unspecified address'],
    [IPAddr.new('::1/128'), 'Loopback address'],
    [IPAddr.new('::ffff:0:0/96'), 'IPv4-mapped addresses'],
    [IPAddr.new('::ffff:0:0:0/96'), 'IPv4 translated addresses'],
    [IPAddr.new('64:ff9b::/96'), 'IPv4/IPv6 translation (RFC6052, NAT64)'],
    [IPAddr.new('64:ff9b:1::/48'), 'IPv4/IPv6 translation (RFC8215)'],
    [IPAddr.new('100::/64'), 'Discard prefix (RFC6666)'],
    [IPAddr.new('200::/7'), 'Reserved by IETF, deprecated as of December 2004'],
    [IPAddr.new('400::/6'), 'Reserved by IETF'],
    [IPAddr.new('800::/5'), 'Reserved by IETF'],
    [IPAddr.new('1000::/4'), 'Reserved by IETF'],
    [IPAddr.new('2000::/3'), 'Global Unicast Address (GUA)'],
    [IPAddr.new('2001::/32'), 'Teredo tunneling'],
    [IPAddr.new('2001:2::/48'), 'Benchmarking (RFC5180)'],
    [IPAddr.new('2001:3::/32'), 'Automatic Multicast Tunneling (AMT) (RFC7450)'],
    [IPAddr.new('2001:4:112::/48'), 'AS112 (RFC7535)'],
    [IPAddr.new('2001:20::/28'), 'ORCHIDv2'],
    [IPAddr.new('2001:db8::/32'), 'Documentation prefix (RFC3849)'],
    [IPAddr.new('2002::/16'), '6to4 (deprecated)'],
    [IPAddr.new('3ffe::/16'), 'Deprecated 6bone range'],
    [IPAddr.new('3ffe:831f::/32'), 'Deprecated range for Teredo tunneling'],
    [IPAddr.new('4000::/3'), 'Reserved by IETF'],
    [IPAddr.new('5f00::/8'), 'Deprecated 6bone range'],
    [IPAddr.new('6000::/3'), 'Reserved by IETF'],
    [IPAddr.new('8000::/3'), 'Reserved by IETF'],
    [IPAddr.new('a000::/3'), 'Reserved by IETF'],
    [IPAddr.new('c000::/3'), 'Reserved by IETF'],
    [IPAddr.new('e000::/4'), 'Reserved by IETF'],
    [IPAddr.new('f000::/5'), 'Reserved by IETF'],
    [IPAddr.new('f800::/6'), 'Reserved by IETF'],
    [IPAddr.new('fc00::/7'), 'Unique local address (ULA)'],
    [IPAddr.new('fe80::/10'), 'Link-local address'],
    [IPAddr.new('fec0::/10'), 'Formerly Site-Local prefix'],
    [IPAddr.new('ff00::/8'), 'Multicast address'],
    [IPAddr.new('ff02::1:ff00:0/104'), 'Solicited-node multicast address'],
    [IPAddr.new('ff02::2:ff00:0/104'), 'Node information queries'],

    # Multicast addresses
    [IPAddr.new('ff01::1/128'), 'All nodes in the interface-local'],
    [IPAddr.new('ff02::1/128'), 'All nodes in the link-local'],
    [IPAddr.new('ff01::2/128'), 'All routers in the interface-local'],
    [IPAddr.new('ff02::2/128'), 'All routers in the link-local'],
    [IPAddr.new('ff05::2/128'), 'All routers in the site-local'],
    [IPAddr.new('ff02::5/128'), 'OSPF'],
    [IPAddr.new('ff02::6/128'), 'OSPF'],
    [IPAddr.new('ff02::9/128'), 'RIP'],
    [IPAddr.new('ff02::a/128'), 'EIGRP'],
    [IPAddr.new('ff02::d/128'), 'RPL'],
    [IPAddr.new('ff01::fb/128'), 'mDNSv6 in the interface-local'],
    [IPAddr.new('ff02::fb/128'), 'mDNSv6 in the link-local'],
    [IPAddr.new('ff03::fb/128'), 'mDNSv6 in the realm-local'],
    [IPAddr.new('ff04::fb/128'), 'mDNSv6 in the admin-local'],
    [IPAddr.new('ff05::fb/128'), 'mDNSv6 in the site-local'],
    [IPAddr.new('ff08::fb/128'), 'mDNSv6 in the organization-local'],
    [IPAddr.new('ff0e::fb/128'), 'mDNSv6 in the global'],
    [IPAddr.new('ff01::101/128'), 'All NTP servers in the interface-local'],
    [IPAddr.new('ff02::101/128'), 'All NTP servers in the link-local'],
    [IPAddr.new('ff03::101/128'), 'All NTP servers in the realm-local'],
    [IPAddr.new('ff04::101/128'), 'All NTP servers in the admin-local'],
    [IPAddr.new('ff05::101/128'), 'All NTP servers in the site-local'],
    [IPAddr.new('ff08::101/128'), 'All NTP servers in the organization-local'],
    [IPAddr.new('ff0e::101/128'), 'All NTP servers in the global'],
    [IPAddr.new('ff02::1:1/128'), 'Link name'],
    [IPAddr.new('ff02::1:2/128'), 'All-dhcp-agents (DHCPv6) in the link-local'],
    [IPAddr.new('ff02::1:3/128'), 'Link-local multicast name resolution'],
    [IPAddr.new('ff05::1:3/128'), 'All-dhcp-agents (DHCPv6) in the site-local'],
    [IPAddr.new('ff02::c/128'), 'SSDP/UPnP link-local'],
    [IPAddr.new('ff05::c/128'), 'SSDP/UPnP site-local'],

    # GUA allocations
    [IPAddr.new('2001::/23'), 'IANA'],
    [IPAddr.new('2001:200::/23'), 'APNIC'],
    [IPAddr.new('2001:400::/23'), 'ARIN'],
    [IPAddr.new('2001:600::/23'), 'RIPE NCC'],
    [IPAddr.new('2001:800::/22'), 'RIPE NCC'],
    [IPAddr.new('2001:c00::/23'), 'APNIC'],
    [IPAddr.new('2001:e00::/23'), 'APNIC'],
    [IPAddr.new('2001:1200::/23'), 'LACNIC'],
    [IPAddr.new('2001:1400::/22'), 'RIPE NCC'],
    [IPAddr.new('2001:1800::/23'), 'ARIN'],
    [IPAddr.new('2001:1a00::/23'), 'RIPE NCC'],
    [IPAddr.new('2001:1c00::/22'), 'RIPE NCC'],
    [IPAddr.new('2001:2000::/19'), 'RIPE NCC'],
    [IPAddr.new('2001:4000::/23'), 'RIPE NCC'],
    [IPAddr.new('2001:4200::/23'), 'AFRINIC'],
    [IPAddr.new('2001:4400::/23'), 'APNIC'],
    [IPAddr.new('2001:4600::/23'), 'RIPE NCC'],
    [IPAddr.new('2001:4800::/23'), 'ARIN'],
    [IPAddr.new('2001:4a00::/23'), 'RIPE NCC'],
    [IPAddr.new('2001:4c00::/23'), 'RIPE NCC'],
    [IPAddr.new('2001:5000::/20'), 'RIPE NCC'],
    [IPAddr.new('2001:8000::/19'), 'APNIC'],
    [IPAddr.new('2001:a000::/20'), 'APNIC'],
    [IPAddr.new('2001:b000::/20'), 'APNIC'],
    [IPAddr.new('2003:0000::/18'), 'RIPE NCC'],
    [IPAddr.new('2400:0000::/12'), 'APNIC'],
    [IPAddr.new('2600:0000::/12'), 'ARIN'],
    [IPAddr.new('2610:0000::/23'), 'ARIN'],
    [IPAddr.new('2620:0000::/23'), 'ARIN'],
    [IPAddr.new('2630:0000::/12'), 'ARIN'],
    [IPAddr.new('2800:0000::/12'), 'LACNIC'],
    [IPAddr.new('2a00:0000::/12'), 'RIPE NCC'],
    [IPAddr.new('2a10:0000::/12'), 'RIPE NCC'],
    [IPAddr.new('2c00:0000::/12'), 'AFRINIC'],
    [IPAddr.new('2d00:0000::/8'), 'IANA'],
    [IPAddr.new('2e00:0000::/7'), 'IANA'],
    [IPAddr.new('3000:0000::/4'), 'IANA'],

    # Multicast use for applications
    [IPAddr.new('ff02::114'), 'Yggdrasil Multicast Discovery'],
    [IPAddr.new('ff02::1:6'), 'Babel routing protocol'],
    [IPAddr.new('224.0.0.111'), 'Babel routing protocol'],
    [IPAddr.new('ff12::8384'), 'Syncthing Local Discovery Protocol'],

    # Overlay networks
    [IPAddr.new('200::/7'), 'Yggdrasil network'],
    [IPAddr.new('fc00::/8'), 'cjdns network'],
    [IPAddr.new('fd00::/8'), 'dn42 / CRXN network'],
    [IPAddr.new('fd42:d42:d42::/48'), 'dn42 anycast'],
    [IPAddr.new('fd10:127::/32'), 'NeoNetwork'],
    [IPAddr.new('2001:db8:dead:beef::/64'), 'router.city network'],

    # Connectivity services with free client
    [IPAddr.new('fd7a:115c:a1e0::/48'), 'Default Tailscale subnet'],
    [IPAddr.new('fd:0:0:4000::/50'), 'Default CloudConnexa WPC Domain Routing subnet'],
    [IPAddr.new('fd:0:0:8000::/49'), 'Default CloudConnexa WPC subnet']
  ].freeze

  def self.getinfo ipaddr
    raise ArgumentError, 'IP address must be of type IPAddr' if ! ipaddr.is_a? IPAddr

    flags = []
    attributes = {}

    attributes['Address'] = ipaddr
    attributes['Next address'] = ipaddr.succ
    attributes['Network'] = "#{ipaddr.mask(ipaddr.prefix)} / #{ipaddr.prefix}"
    attributes['Netmask'] = ipaddr.netmask

    if ipaddr.ipv4?
      flags << 'IPv4'

      ADDRESSES_4.each do |ip, descr|
        flags << descr if ip.include? ipaddr
      end

      attributes['IPv4-mapped IPv6'] = "::ffff:#{ipaddr}"

      [32, 27, 24, 16].each do |mask|
        bits = mask - ipaddr.prefix
        if bits.positive?
          num_prefix = 2**bits
          calc_res = " = #{num_prefix}" if num_prefix <= 65_536
          attributes["/#{mask}s"] = "2^#{bits}#{calc_res}"
        end
      end
    elsif ipaddr.ipv6?
      flags << 'IPv6'

      if ipaddr.ipv4_mapped?
        attributes['IPv4'] = ipaddr.native
        attributes['Alternative address'] = ":#{ipaddr.inspect[39...-41]}"
      end

      attributes['Meshname'] = "#{Meshname.getname ipaddr}.meship"

      [128, 64, 56, 48].each do |mask|
        bits = mask - ipaddr.prefix
        if bits.positive?
          num_prefix = 2**bits
          calc_res = " = #{num_prefix}" if num_prefix <= 65_536
          attributes["/#{mask}s"] = "2^#{bits}#{calc_res}"
        end
      end

      ADDRESSES_6.each do |ip, descr|
        flags << descr if ip.include? ipaddr
      end
    else
      flags << 'Unknown IP version'
    end

    attributes['rDNS'] = ipaddr.reverse

    range = ipaddr.to_range
    attributes['Host Range'] = "{ #{range.first} - #{range.last} }"

    return [attributes, flags]
  end

end
