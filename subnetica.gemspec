
Gem::Specification.new do |spec|
  spec.name = 'subnetica'
  spec.version = '1.1.4'
  spec.summary = 'Program that can display IPv4 and IPv6 address information.'
  spec.description = 'Program that can display IPv4 and IPv6 address information like Network, Netmask, rDNS address and Host Range.'
  spec.authors = ['Marek Küthe']
  spec.email = 'm.k@mk16.de'
  
  spec.files = %w[lib/subnetica.rb]
  spec.executables = %w[subnetica]
  spec.extra_rdoc_files = %w[LICENSE README.md]
  
  spec.homepage = 'https://codeberg.org/mark22k/subnetica'
  spec.license = 'WTFPL'
  
  spec.metadata = { 'source_code_uri' => 'https://codeberg.org/mark22k/subnetica',
                    'bug_tracker_uri' => 'https://codeberg.org/mark22k/subnetica/issues',
                    'rubygems_mfa_required' => 'true' }
  
  spec.required_ruby_version = '>= 3.1'
  spec.add_runtime_dependency 'meshname', '~> 1.0', '>= 1.0.0'
end
