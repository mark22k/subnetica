# Credits

Inspired by [subnetcalc](https://packages.debian.org/search?searchon=sourcenames&keywords=subnetcalc).

## Links

- [Is there any difference between Link-local and Site-local scope in IPv6 world?](https://networkengineering.stackexchange.com/questions/47378/is-there-any-difference-between-link-local-and-site-local-scope-in-ipv6-world)
- [IPv6 address](https://wikiless.org/wiki/IPv6_address?lang=en)
- [Reserved IP addresses](https://wikiless.org/wiki/Reserved_IP_addresses?lang=en)
